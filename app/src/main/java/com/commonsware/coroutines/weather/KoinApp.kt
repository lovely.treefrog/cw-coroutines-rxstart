/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.weather

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle

class KoinApp : Application() {
  private val koinModule = module {
    single { OkHttpClient.Builder().build() }
    single { ObservationDatabase.create(androidContext()) }
    single { ObservationRemoteDataSource(get()) }
    single<IObservationRepository> { ObservationRepository(get(), get()) }
    single { DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT) }
    viewModel { MainMotor(get(), get(), androidContext()) }
  }

  override fun onCreate() {
    super.onCreate()

    AndroidThreeTen.init(this);

    startKoin {
      androidLogger()
      androidContext(this@KoinApp)
      modules(koinModule)
    }
  }
}